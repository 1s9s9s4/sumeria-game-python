# Sumeria

Sumeria is a text based game from the 60s/70s. I am implementing in python, the version Mike Arnautov implementd in C in 1975. 
Do let me know if something needs fixing. I wrote this to pass my time on a Sunday. Also, I tried to stick to the original
as much as possible (including 80 character line limits).