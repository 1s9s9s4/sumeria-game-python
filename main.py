import math
import time
import sys
from random import uniform

#globals
dead_total = 0
percent_starved = 0
year_term = 0
year_abs = 0
population = 100
starved = 0

def abs(i):
    if i>=0:
        return i
    else:
        return -i


def terminate(abort):
    if abort == 2:
        print "For this extreme mismanagement you have been"
        print "deposed, flayed alive and publicly beheaded."
        print "\nMay Ashtaroth preserve your Ka.\n"

    elif abort == 1:
        print "\nHamurabe:  I find myself unable to fulfil your wish."
        print "You will have to find yourself another kingdom."

    if abort !=2 :
        print "\nMay Baal be with you\n"

def query(prompt):
    v = raw_input(prompt).strip()
    s = unicode(v,'utf-8')
    if v == 'q' or v == 'Q':
        terminate(1)
    elif s.isnumeric() == True :
        return int(v)
    else:
        print "Hamurabe, your command has not been understood!"

def think_again(what,quantity):
    if what == 'land' or what == 'grain':
        print "Hamurabe, thing again"

    if what == 'land':
        print "You own only",quantity,"acres of land"
    elif what == 'grain':
        print "You have only",quantity,"bushels of grain"
    else:
        print "You only have",population,"people to tend to the fields"
    print " Now then, "

def try_again(reason):
    if reason == 1:
        print "For the extreme folly of soft - heartedness and"
    if reason!=0:
        if reason == 3:
            cap = 'C'
        else:
            cap = 'c'

        print cap+"onsidering the mess you would leave the city in,"
        print "you are hereby commanded to remain in office"
        print "for another ten years. May your fate be a"
        print "lesson and a warning for generations to come."
    else:
        print "Hamurabe, you are either a politico-economic"
        print "genius or just a lucky bastard. There being"
        print "but one way to settle the question, you are"
        print "hereby requested to stay in office"
        print "for another ten years"
    global year_term
    year_term = 0
    global year_abs
    year_abs -= 1
    global percent_starved
    global population
    global starved
    percent_starved = starved * 100 / population
    global dead_total
    dead_total = starved

def main():
    print "[Sumeria (Primos) rev.19.1, GGR (MLA) version 14 Oct 83]"
    print "[Conversion to ANSI C: MLA, Geb 2002]"
    print "[Conversion to Python 2.7, April 23, 2017]"
    # Ask players if they know how to play
    while True:
        print "Do you know how to play? ",
        reply = raw_input()
        if reply == '':
            break
        elif reply == 'y':
            break
        elif reply != 'n' and reply !='q':
            continue
        else:
            print "\nToo bad!"
            break
    print "Try your hand at governing ancient Sumeria"
    print "for a ten year term of office"

    #Global Variables:
    global year_term
    global year_abs
    global percent_starved
    global starved
    global dead_total
    global population

    #Initialisation
    second_term = 0
    dead_total = 0
    percent_starved = 0
    year_term = 0
    year_abs = 0
    acres_per_init = 10
    population = 100
    stores = 2800
    harvest = 3000
    rat_food = 200
    _yield = 3
    acreage = 1000
    immigration = 5
    transaction = 0
    price = 18 + 6 * uniform(0,1)
    breadline = 19 + 4 * uniform(0,1)
    provisions = breadline
    rats = 1000
    rat_log = 3
    plague = uniform(0,2)/2
    starved = 0
    year_term = year_abs = 0
    plague_deaths =  population * (0.429 * plague - 0.164)
    # Gameplay begins

    while True:
        while True:
            year_term += 1
            year_abs += 1
            acres_per_head = acreage / population
            stores_per_head = stores / population
            print "Hamurabe: I beg to report to you,"
            print "In year",year_abs,
            #Starvation report
            if starved > 0:
                print starved,
            else:
                print "no",

            if starved >= 1:
                print "people",
            else:
                print "person",
            print "starved,"
            #Immigration report
            print immigration,"people came to the city"

            #Plague report if it happened. plague_deaths ???
            if plague >= 0.85:
                print "A horrible plague struck!",plague_deaths

            print "Population is now",population
            print "The city owns",acreage,"acres"
            print "You harvested",_yield,"bushels per acre"
            print "Rats ate",rat_food,"bushels"
            print "You now have",stores,"bushels in store.\n"

            if year_term == 11:
                print "breaking out of INNER LOOP"
                break

            rounded_price = price + 0.5
            print "Land is trading at",rounded_price,"bushels per acre\n"

            while True:
                buy = query("How many acres do you wish to buy? ")
                if rounded_price * buy <= stores:
                    break
                else:
                    think_again("grain",stores)

            if buy > 0:
                acreage += buy
                stores -= rounded_price * buy
                transaction = buy
            else:
                while True:
                    sell = query("How many acres do you wish to sell? ")
                    if sell <= acreage:
                        break
                    else:
                        think_again("land",acreage)

                acreage -= sell
                stores += rounded_price * sell
                transaction -= sell

            print ''

            while True:
                food = query("How many bushels do you wish to feed your people? ")
                if food <= stores:
                    break
                think_again("grain",stores)

            stores -= food
            print ''

            while True:
                plant = query("How many acres do you wish to plant with seeds? ")
                if plant <= acreage and plant <= 2*stores and plant <= 10*population:
                    break
                if plant > acreage:
                    think_again("land",acreage)
                elif plant > 2*stores:
                    think_again("grain",stores)
                else:
                    think_again("people",population)

            stores -= plant/2
            _yield = 4 * uniform(0,1) + 1.65
            harvest = plant * _yield
            rat_food = 0
            rats_ate = stores * (rat_log - 2.2)/3.6
            dead_rats = rats - 4*rats_ate
            rats = 3*rats
            if dead_rats > 0:
                rats = rats - dead_rats

            if plague >= 0.3:
                if plague >= 0.85:
                    if plague > 1:
                        plague = 1
                    rats = 500 + 5000 * (plague - 0.7)

                else:
                    rats *= (1.225 - 0.75*plague)

            if rats < 500:
                rats = 500

            rat_food = rats/4

            if rats_ate < rat_food:
                rat_food = rats_ate
            rat_food *= 7
            if rat_food <= 20:
                rat_food = 20 + 30 * uniform(0,1)
            stores += harvest - rat_food

            rat_log = math.log10(rats)

            if stores+stores <= harvest:
                rat_food = harvest * (1+uniform(0,1))/4.0
                stores = harvest - rat_food

            tmp_int = 100 + abs(100 - population)
            immigration = tmp_int * ((acres_per_head + stores_per_head - 36)/250 + (provisions - breadline + 2.5)/40) + 0.5
            if immigration <= 0:
                immigration = 5*uniform(0,1)+ 1
            survived = food/breadline
            provisions = food/population
            plague = (2* uniform(0,1) + rat_log - 3)/3.0
            if population < survived:
                survived = population
            else:
                starved = population - survived
                if starved >= 0.45*population:
                    print "You have starved",starved,"people in one year"
                    terminate(1)
                percent_starved = ((year_term - 1) * percent_starved +
                   100.0 * starved / population) / year_term
                population = survived
                dead_total += starved

            population += immigration
            price = (price + 15 + (stores_per_head - acres_per_head) / 3)/2 +        transaction / 50 + 3 * uniform(0,1) - 2

            if price < 1.0:
                price = 1.0

            if plague >= 0.85:
                plague_deaths = population * (0.429 * plague - 0.164)
                population -= plague_deaths


        print "In your ten year term of office",math.floor(dead_total),"people starved"
        print "You started with", acres_per_init,"acres per person and ended",
        acres_per_head = acreage/population
        acres_per_init = acres_per_head
        print "with",acres_per_head,"per person"

        tmp_float = 10 * acres_per_head/3
        if percent_starved > 25:
            terminate(2)

        if percent_starved <= 7:
            try_again(1)
            continue

        if tmp_float < 7:
            terminate(2)

        if tmp_float > 10:
            print "Your heavy handed performance smacks of Nabuchodonoser"
            print "and Asurbanipal II. The surviving populace"
            print "hates your guts and your eventual"
            print "assasination is just a matter of time"
            terminate(0)
        print "Consequently you have been deposed and disgraced"
        print "and only avoided a public punishment because of"
        print "mitigating circumstances. While it may be"
        print "admitted in private that you had a rotten deal "
        tmp_int = int(3 * uniform(0,1))
        if tmp_int == 0:
            print "try explaining that to a mob looking for scape-goats."
        elif tmp_int == 1:
            print "history is not interested in such petty excuses."
        else:
            print "you should have considered such occupational hazards"
            print "before applying for the job."
        terminate(0)

        if acres_per_head < 7:
            try_again(1)
            continue

        if acres_per_head < 9:
            print "Your performance has been satisfactory and, in the"
            print "perspective of history, actually quite good."
            if uniform(0,1) >= 0.5:
                print "You may not be exactly popular, but given a good"
                print "body-guard there is nothing to be really worried about."
            else:
                print "While not exactly loved, you are at least respected."
                print "What more can a realistic ruler ask for?"
        elif second_term == 0:
            if stores <= 10 * population:
                try_again(3)
                continue
            second_term = 1
            try_again(0)
            continue
        else:
            print "Hamurabe, your name will be remembered through the"
            print "ages to come with admiration and respect.\n"
            print "(So you did get away with it you lucky sod!)"

        if stores > 10 *population:
            terminate(0)
            print "\n                            HOWEVER\n\n"
            second_term = 0
            try_again(2)
            continue











if __name__ == '__main__':
    main()
